# Installation on Windows

Since python and Windows are not as straight forward to use, here is a small guide.

## Installing Python

First install [`python`](https://www.python.org/downloads/windows/) from the official website. When the installer asks if python should be added to `PATH`, click yes.

## Installing the dependencies

SABS uses several other open source modules to function. You have to install them, in order to use SABS. 

- [`scipy`](https://scipy.org/install/)
- [`numpy`](https://numpy.org/install/)
- [`uncertainties`](https://uncertainties.readthedocs.io/en/latest/#installation-and-download)
- [`matplotlib`](https://matplotlib.org/stable/#installation)

On Windows this is done using the `pip` module. In case you are using a Computer that is behind a Proxy (like is often the case at a reasearch institution) you need to set the proxy in your environment variables. 
To do this on Windows:
1. In the Start menu, search for “env”.
2. Select “Edit Environment Variables **for your account**”
3. In the upper section add two variables `http_proxy` and `https_proxy` both with the address of the proxy server of your institute  
e.g.: `http://adress-of-proxy.instute.example:port`


Now open a PowerShell prompt from the Start menu and type:

```PowerShell
python -m pip install --upgrade scipy numpy uncertainties matplotlib
```

## Installing SABS

SABS is just a script for now and does not have to be installed. This is done, so you the researcher have the opportunity to check and modify the sourcecode more easily.

[Download](https://gitea.ifw-dresden.de/b.mehlhorn/sabs/archive/master.zip) the SABS Repository from Gitea as a `.zip` file and extract it. If you know how, you can also clone the repo of course. 

Now open the PowerShell in the same directory as `sabs.py` by finding it in the extracted files and pressing `Alt-D` and then `P`. Now type the following command to test the software:

```PowerShell
python sabs.py -p `
-s sampledata/2021-11-08_Fe5Ge2Te5_SE5749_grease-quartz-brass_MvT_1000Oe.rw.dat `
-b sampledata/2021-11-04_Test_background_grease-quartz-brass_MvT_1000Oe.rw.dat `
-o sampledata/2021-12-17_Fe5Ge2Te5_SE5749_grease-quartz-brass_MvT_1000Oe.sabs.dat
```

If you see the Window showing the sample scans you are done. You can now copy the file `sabs.py` near your data and replace the filenames in the `-s`, `-b` and `-o` flags to your own.

